# UnityJobExperiments

**Disclaimer** 
This repo does **NOT** contain production ready code, use at your own risk. 

In this repo I'll make various experiments with the Unity Engines job system. 

Current unity version used **2018.1.0b9**

Link to live webstream: [Webcam stream](http://96.81.44.121/mjpg/video.mjpg?timestamp=1520024196410)

# Grayscale

The file ([Grayscale.cs](https://goo.gl/RJ9yso)) contains a very simple implementation of _IJobParallelFor_ that converts a color texture into a grayscale texture. 


# Median/Mean Experiement 

The file ([Median.cs](https://goo.gl/Nc5jTN)) contains two experiments. Mean and Median(-ish). Both experiements use 30 webcam snapshots at around 1M pixels each.
