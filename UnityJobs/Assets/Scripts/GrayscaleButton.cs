﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

public class GrayscaleButton : MonoBehaviour
{

    public RawImage ColorImage;
    public RawImage GrayscaleImage;

    private void Start()
    {
        //Lovely.. UGUI Button are broken in unity 2018.1.0b9 on canvas set to screen space camera.

  //      UnityAction onButtonClicked =
  //          () =>
  //      {
  //          Debug.Log("Click");
  //          Grayscale.CreateGrayscale(ColorImage.texture as Texture2D, grayscaleTexture => GrayscaleImage.texture = grayscaleTexture);
  //      };    

		//var buttion = GetComponent<Button>();
        //buttion.onClick.AddListener(onButtonClicked);
        //buttion.onClick.AddListener(() => Debug.Log("Click"));
    }

    private void OnGUI()
    {
        if(GUI.Button(new Rect(0,0,100,100), "Calculate Gray"))
            Grayscale.CreateGrayscale(ColorImage.texture as Texture2D, grayscaleTexture => GrayscaleImage.texture = grayscaleTexture);
    }

}
