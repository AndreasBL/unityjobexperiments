﻿using System;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

public class Grayscale : MonoBehaviour
{
    private NativeArray<Color32> nativeColors;
    private NativeArray<Color32> nativeGray;

    private JobHandle jobHandle;

    private DateTime startTime;


    public static void CreateGrayscale(Texture2D colorTexture, Action<Texture2D> onCalculationComplete)
    {
        var gameObject = new GameObject("CreateGrayscale");
        var grayscaleComponent = gameObject.AddComponent<Grayscale>();
        grayscaleComponent.StartCalculation(colorTexture, onCalculationComplete);
    }

    private void StartCalculation(Texture2D colorTexture, Action<Texture2D> onCalculationComplete)
    {
        var rawColors = colorTexture.GetPixels32();
        nativeColors = new NativeArray<Color32>(rawColors.Length, Allocator.Temp);
        nativeColors.CopyFrom(rawColors);

        nativeGray = new NativeArray<Color32>(rawColors.Length, Allocator.Temp);

        var calculateGrayJob = new CalculateGrayScale
        {
            colors = nativeColors,
            grayScale = nativeGray
        };

        startTime = DateTime.Now;

        jobHandle = calculateGrayJob.Schedule(rawColors.Length, innerloopBatchCount: 64);
        jobHandle.Complete();

        Debug.Log("Job completed in " + DateTime.Now.Subtract(startTime).TotalSeconds + " seconds");

        //This is rather misleading, as the job might have been done for almost a frame.. 

        var outputGrayScaleTexture = new Texture2D(colorTexture.width, colorTexture.height);
        outputGrayScaleTexture.SetPixels32(calculateGrayJob.grayScale.ToArray());
        outputGrayScaleTexture.Apply(false, true);

        if (onCalculationComplete != null)
            onCalculationComplete(outputGrayScaleTexture);

        Destroy(this.gameObject);
    }


    struct CalculateGrayScale : IJobParallelFor
    {
        [ReadOnly]
        public NativeArray<Color32> colors;

        public NativeArray<Color32> grayScale;

        public void Execute(int index)
        {
            var color = colors[index];

            var grayValue = (byte) ((color.r + color.g + color.b) / 3f); //I know that the colors normally isn't weighted equal... this is just for demo purpose. 

            grayScale[index] = new Color32(grayValue, grayValue, grayValue, 255);
        }
    }

    private void OnDestroy()
    {
        nativeGray.Dispose();
        nativeColors.Dispose();
    }
}
