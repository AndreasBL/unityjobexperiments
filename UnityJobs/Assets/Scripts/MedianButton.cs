﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MedianButton : MonoBehaviour
{
    public List<Texture2D> InputTextures;
    public RawImage MedianImage;

    private void OnGUI()
    {
        if(GUI.Button(new Rect(0,0,100,100), "Calculate Gray"))
            Median.CreateMedian(InputTextures, grayscaleTexture => MedianImage.texture = grayscaleTexture);
    }

}
