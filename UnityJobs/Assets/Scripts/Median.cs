﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;


public class Median : MonoBehaviour
{
    private NativeArray<Color32> nativeColors;
    private NativeArray<Color32> nativeMedian;

    public static ArrayPool<Color32> ColorArray { private set; get; }

    public static void CreateMedian(List<Texture2D> colorTextures, Action<Texture2D> onCalculationComplete)
    {
        var gameObject = new GameObject("CreateGrayscale");
        var grayscaleComponent = gameObject.AddComponent<Median>();
        grayscaleComponent.StartCalculation(colorTextures, onCalculationComplete);
    }

    private void StartCalculation(List<Texture2D> colorTextures, Action<Texture2D> onCalculationComplete)
    {
        var pixelsInTextures = colorTextures[0].width * colorTextures[0].height;
        var totalNumberOfColors = colorTextures.Count * pixelsInTextures;

        //Fun thing to do... jobify the contruction of the flatten array
        var allColors = colorTextures.Select(c => c.GetPixels32())
                                     .SelectMany(e => e) //Flatten the enumerators
                                     .ToArray();
        
		Debug.Log("Number of pixel: " + allColors.Length);

        nativeColors = new NativeArray<Color32>(allColors, Allocator.Temp);
        nativeMedian = new NativeArray<Color32>(pixelsInTextures, Allocator.Temp);

        var batchSize = 1;
        var calculateGrayJob = new CalculateGrayScale
        {
            colors = nativeColors,
            medianColors = nativeMedian,
            PixelsInTextures = pixelsInTextures,
            NumberOfTextures = colorTextures.Count
        };

        var startTime = DateTime.Now;

        ColorArray = new ArrayPool<Color32>(batchSize, colorTextures.Count);
        var jobHandle = calculateGrayJob.Schedule(pixelsInTextures, innerloopBatchCount: batchSize);

        //I am just lazy, and don't wanna pass alot of params to a method.. ;) 
        Action onJobComplete = () =>
        {
			Debug.Log("Job completed in " + DateTime.Now.Subtract(startTime).TotalSeconds + " seconds");
			
			//Return result texture
			var outputGrayScaleTexture = new Texture2D(colorTextures[0].width, colorTextures[0].height);
			outputGrayScaleTexture.SetPixels32(calculateGrayJob.medianColors.ToArray());
			outputGrayScaleTexture.Apply(false, true);
			
			if (onCalculationComplete != null)
				onCalculationComplete(outputGrayScaleTexture);
			
			Destroy(this.gameObject);
        };

        //See note for method below
        StartCoroutine(WaitEndOfFrameThenComplete(jobHandle, onJobComplete));
    }

    /// <summary>
    /// https://docs.unity3d.com/2018.1/Documentation/ScriptReference/Unity.Jobs.IJobParallelFor.html
    /// According to the notes on the link above,
    /// there is a larger chance of parallelism with potential other jobs if the job is not completed immediately. 
    /// </summary>
    private IEnumerator WaitEndOfFrameThenComplete(JobHandle jobHandle, Action onComplete)
    {
        yield return new WaitForEndOfFrame();

        jobHandle.Complete();
        onComplete();
    }


    private struct CalculateGrayScale : IJobParallelFor
    {
        [ReadOnly]
        public int PixelsInTextures;

        [ReadOnly]
        public int NumberOfTextures; //No need to recalculate this all the time

        [ReadOnly]
        public NativeArray<Color32> colors;

        public NativeArray<Color32> medianColors;

        public void Execute(int index)
        {
            //medianColors[index] = Mean(index);
            medianColors[index] = CalculateMedian(index);
        }

        private Color32 CalculateMedian(int index)
        {
            //the same pixel in every texture
            //var samePixels = new Color32[NumberOfTextures]; //This is very expensive..
            var samePixels = Median.ColorArray.GetArrayFromPool();

            int count = 0;
            for (int i = index; i < NumberOfTextures * PixelsInTextures; i += PixelsInTextures)
                samePixels[count++] = colors[i];

            //Array.Sort(samePixels, (c0, c1) => Length(c0).CompareTo(Length(c1)));
            Array.Sort(samePixels, (c0, c1) => c0.r.CompareTo(c1.r)); // Only red

            var medianColor = samePixels[(int)(NumberOfTextures * 0.5f)]; //take the pixel in the middle-ish. Yeah this is not the median.

            Median.ColorArray.PutArrayInPool(samePixels);

            return medianColor;
        }

        private Color32 Mean(int index)
        {
            var colorMean = new float[3];//This is very expensive..

            for (int i = index; i < NumberOfTextures * PixelsInTextures; i += PixelsInTextures)
            {
                colorMean[0] += colors[i].r;
                colorMean[1] += colors[i].g;
                colorMean[2] += colors[i].b;
            }

            for (int i = 0; i < colorMean.Length; i++)
                colorMean[i] /= NumberOfTextures;

            return new Color32((byte)colorMean[0], (byte)colorMean[1], (byte)colorMean[2], 255);
        }

        //private static float Length(Color32 color)
        //{
        //    return Mathf.Pow(color.r, 2) + Mathf.Pow(color.g, 2) + Mathf.Pow(color.b, 2);
        //}
    }

    private void OnDestroy()
    {
        nativeMedian.Dispose();
        nativeColors.Dispose();
    }
}