﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;


public class ArrayPool<T> where T : struct
{
    private readonly ConcurrentStack<T[]> pool;
    private readonly int arraySize;

  
    public ArrayPool(int initSize, int arraySize)
    {
        Debug.Log("Creating pool with initSize " + initSize + " arraySize: " + arraySize);
        this.arraySize = arraySize;

        pool = new ConcurrentStack<T[]>(new List<T[]>(initSize * 2));

        for (int i = 0; i < initSize; i++)
            pool.Push(new T[arraySize]);
    }

    public T[] GetArrayFromPool()
    {
        T[] array;
        if (pool.TryPop(out array))
            return array;

        return new T[arraySize];
    }

    public void PutArrayInPool(T[] oldArray)
    {
        pool.Push(oldArray);
    }
}


